from django.contrib import admin
from django.urls import path, include
import os

urlpatterns = [
    path('weathersound/admin/', admin.site.urls),
    path(f"weathersound/api/v{os.getenv('VERSION', default='1')}/", include('api.urls'))
]
